<div align="center">

  <h1><code>wasm-game-of-life</code></h1>

  <strong>This is my extended version of the <a href="https://rustwasm.github.io/book">WASM Game of Life example project</a>.</strong>

</div>

## 📚 About

This project is based on the aforementioned WASM Game of Life example project.

## 🎮 Features

* Start/Stop button
* Random universe generation
* Clear button
* Delay slider
* Change universe size
* Click to toggle cells
* Hold mouse button and drag to toggle multiple cells
* Works without Node.js / npm

## 🗺 Roadmap

* Replace JavaScript with Rust/WASM -> [Yew](https://yew.rs)!
* Make universe unbounded
* Implement hashlife
* Upload and insert patterns
* Download in different formats (picture, [RLE](https://www.conwaylife.com/wiki/Run_Length_Encoded), maybe others)
* Import rules (by rule string/integers)

## 🚲 Usage

### 🛠️ Needed tools

* [Standard rust toolchain](https://www.rust-lang.org/tools/install)
* [wasm-pack](https://rustwasm.github.io/wasm-pack/installer/)


### 📦 Build with `wasm-pack build`

```
wasm-pack build --target web
```

### 🔍 Use the debug flag for tracing errors

```
wasm-pack build --debug --target web
```

### 🎬 Run on test server with `cargo run --example deploy`

```
cargo run --example deploy
```

### ⚙ Test in Headless Browsers with `wasm-pack test`

Note: There are no tests yet
```
wasm-pack test --headless --firefox
```
